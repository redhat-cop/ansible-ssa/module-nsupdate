#!/usr/bin/python
#
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
#

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = r'''
---
module: a_record

short_description: This module can create a DNS A records by using nsupdate

# If this is part of a collection, you need to use semantic versioning,
# i.e. the version is of the form "2.5.0" and not "2.4".
version_added: "1.0.0"

description: This module can create DNS A records by using nsupdate.

options:
    name:
        description: The short name of the DNS A record.
        required: true
        type: str
    ipaddress:
        description: The IP address the DNS A record points to
        required: true
        type: str
    key:
        description: The key data used to authenticate with the DNS server, typically this is the content of the key file.
        required: true
        type: str
    private:
        description: The private key data used to authenticate with the DNS server, typically this is the content of the private file.
        required: true
        type: str
    suffix:
        description: THe DNS sub domain used when creating the DNS A record
        required: true
        type: str
# Specify this value according to your collection
# in format of namespace.collection.doc_fragment_name
extends_documentation_fragment:
    - ansible_ssa.nsupdate.a_record

author:
    - Christian Jung (software@jung-christian.de)
'''

EXAMPLES = r'''
# Pass in a message
- name: Create DNS A record host1.example.ansible-labs.de with IP 192.168.1.1
  ansible_ssa.nsupdate.a_record:
    name: host1
    ipaddress: 192.168.1.1
    key: <key data>
    private: <private key data>
    suffix: example
'''

from ansible.module_utils.basic import AnsibleModule
import subprocess
import tempfile
import socket

def write_to_file(filename,data):
    # helper function to write given data to file
    filename = open(filename,mode='w')
    filename.write(data)
    filename.flush()
    return(filename)

def create_dns_record(result,module,fqdn):
    # manipulate or modify the state as needed (this is going to be the
    # part where your module will do what it needs to do)

    # create a temporary directory
    with tempfile.TemporaryDirectory() as tempdir:
        # write nsupdate commands to file
        nsupdate="server ns.ansible-labs.de\nzone "+module.params['suffix']+".ansible-labs.de."
        nsupdate+="\nupdate delete "+fqdn+"."
        nsupdate+="\nupdate add "+fqdn+". 60 A "+module.params['ipaddress']+"\n"
        nsupdate+="\nsend\n"
        nsupdatefile=write_to_file(tempdir+"/nsupdate",nsupdate)

        # write key to file
        keyfile=write_to_file(tempdir+"/nsupdate.key", module.params['key'])

        # write private key to file
        privatekeyfile=write_to_file(tempdir+"/nsupdate.private", module.params['private'])

        # Define command as string and then split() into list format
        cmd = "/usr/bin/nsupdate -k "+tempdir+"/nsupdate.key "+nsupdatefile.name

        # Use shell to execute the command, store the stdout and stderr in sp variable
        sp = subprocess.Popen(cmd,
                shell=True,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                universal_newlines=True)

        # Store the return code in rc variable
        rc=sp.wait()

        # Separate the output and error by communicating with sp variable.
        # This is similar to Tuple where we store two values to two different variables
        out,err=sp.communicate()

        if rc != 0:
            module.fail_json(msg="Something didn't work (return code from nsupdate: "+str(rc)+" Error message:"+err+")", **result)

def run_module():
    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        name=dict(type='str', required=True),
        ipaddress=dict(type='str', required=True),
        key=dict(type='str', required=True),
        private=dict(type='str', required=True),
        suffix=dict(type="str", required=True)
    )

    # seed the result dict in the object
    # we primarily care about changed and state
    # changed is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(
        changed=False,
        original_message='',
        message=''
    )

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=False
    )

    # if the user is working with this module in only check mode we do not
    # want to make any changes to the environment, just return the current
    # state with no modifications
    if module.check_mode:
        module.exit_json(**result)

    # check current DNS record
    # this will make the module idempotent

    # build FQDN for DNS record
    fqdn=module.params['name']+"."+module.params['suffix']+".ansible-labs.de"
    dns_ipadddress=""
    try:
        dns_ipadddress = socket.gethostbyname(fqdn)
    except:
        # DNS record does not exist
        create_dns_record(result,module,fqdn)
        result['original_message'] = module.params['name']
        result['message'] = 'DNS record created ' + fqdn + ' and set to ' + module.params['ipaddress']
        result['changed'] = True

    if dns_ipadddress != module.params['ipaddress']:
        # IP address does not match
        create_dns_record(result,module,fqdn)
        result['original_message'] = module.params['name']
        result['message'] = 'DNS updated ' + fqdn + ' to ' + module.params['ipaddress']
        result['changed'] = True
    else:
        result['original_message'] = module.params['name']
        result['message'] = 'DNS already up to date'
        result['changed'] = False

    # during the execution of the module, if there is an exception or a
    # conditional state that effectively causes a failure, run
    # AnsibleModule.fail_json() to pass in the message and the result
    #if module.params['name'] == 'fail me':
    #    module.fail_json(msg='You requested this to fail', **result)

    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results
    module.exit_json(**result)

def main():
    run_module()

if __name__ == '__main__':
    main()
